const Project = require('../../models/project')
const { createItem } = require('../../middleware/db')
const { handleError } = require('../../middleware/utils')
const { matchedData } = require('express-validator')
const { projectExists } = require('./helpers')

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createProject = async (req, res) => {
  try {
    req = matchedData(req)
    const doesProjectExists = await projectExists(req.name)
    if (!doesProjectExists) {
      res.status(201).json(await createItem(req, Project))
    }
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createProject }
