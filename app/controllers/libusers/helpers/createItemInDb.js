/* eslint-disable camelcase */
const uuid = require('uuid')
const LibUser = require('../../../models/libUser')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Creates a new item in database
 * @param {Object} req - request object
 */
const createItemInDb = ({
  name = '',
  surname = '',
  email = '',
  password = '',
  role = '',
  academicPrefix = '',
  prefix = '',
  ratenumber = '',
  position = '',
  affiliate = '',
  division = '',
  branch = '',
  idCard = '',
  birthdate = '',
  gender = '',
  children = '',
  religion = '',
  status = '',
  blood = '',
  domicile = '',
  address = '',
  serviceday = '',
  work = '',
  nationality = '',
  ethnicity = '',
  phone = '',
  educations = [],

}) => {
  return new Promise((resolve, reject) => {
    const libUser = new LibUser({
      name,
      surname,
      email,
      password,
      role,
      academicPrefix,
      prefix,
      ratenumber,
      position,
      affiliate,
      division,
      branch,
      idCard,
      birthdate,
      gender,
      children,
      religion,
      status,
      blood,
      domicile,
      address,
      serviceday,
      work,
      nationality,
      ethnicity,
      phone,
      educations,
      verification: uuid.v4()
    })
    libUser.save((err, item) => {
      if (err) {
        reject(buildErrObject(422, err.message))
      }

      item = JSON.parse(JSON.stringify(item))

      // delete item.password
      // delete item.blockExpires
      // delete item.loginAttempts

      resolve(item)
    })
  })
}

module.exports = { createItemInDb }
