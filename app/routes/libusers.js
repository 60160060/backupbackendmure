const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const { roleAuthorization } = require('../controllers/auth')

const {
  getLibusers,
  createLibuser,
  getLibuser,
  updateLibuser,
  deleteLibuser,
  createEducation
} = require('../controllers/libusers')

const {
  validateCreateLibuser,
  validateGetLibuser,
  validateUpdateLibuser,
  validateDeleteLibuser,
  validateCreateEducation
} = require('../controllers/libusers/validators')

/*
 * Users routes
 */

/*
 * Get items route
 */
router.get(
  '/',
  // requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  getLibusers
)

/*
 * Create new item route
 */
router.post(
  '/',
  // requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateCreateLibuser,
  createLibuser
)

router.post(
  '/education',
  // requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateCreateEducation,
  createEducation
)

/*
 * Get item route
 */
router.get(
  '/:id',
  // requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateGetLibuser,
  getLibuser
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  // requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateUpdateLibuser,
  updateLibuser
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  roleAuthorization(['admin']),
  trimRequest.all,
  validateDeleteLibuser,
  deleteLibuser
)

module.exports = router
