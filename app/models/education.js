const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const mongoosePaginate = require('mongoose-paginate-v2')

const EducationSchema = new mongoose.Schema(
  {
    educations: {
      name: {
        type: String
      },
      abbreviation: {
        type: String
      },
      major: {
        type: String
      },
      year: {
        type: String
      },
      institution: {
        type: String
      }
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

const hash = (user, salt, next) => {
  bcrypt.hash(user.password, salt, (error, newHash) => {
    if (error) {
      return next(error)
    }
    user.password = newHash
    return next()
  })
}

const genSalt = (user, SALT_FACTOR, next) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

EducationSchema.pre('save', function (next) {
  const that = this
  const SALT_FACTOR = 5
  if (!that.isModified('password')) {
    return next()
  }
  return genSalt(that, SALT_FACTOR, next)
})

EducationSchema.methods.comparePassword = function (passwordAttempt, cb) {
  bcrypt.compare(passwordAttempt, this.password, (err, isMatch) =>
    err ? cb(err) : cb(null, isMatch)
  )
}

EducationSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Education', EducationSchema)
