const faker = require('faker')
const ObjectID = require('mongodb').ObjectID

module.exports = [
  {
    _id: new ObjectID('5fdc5dd7569081471870fe11'),
    name: 'อัญชสา',
    surname: 'ขำอยู่',
    email: 'admin@admin.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'admin',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f',
    academicPrefix: 'ศาสตราจารย์',
    prefix: 'นางสาว',
    ratenumber: '0917',
    position: 'ผู้ปฏิบัติงานห้องสมุด',
    affiliate: 'สำนักหอสมุด',
    division: 'ฝ่ายพัฒนาทรัพยากรสารสนเทศ',
    branch: '-',
    idCard: '3200100609018',
    birthdate: faker.date.past(),
    gender: 'หญิง',
    children: '-',
    religion: 'ศาสนาพุทธ',
    status: 'โสด',
    blood: 'เอ',
    domicile: 'มหาวิทยาลัยบูรพา ตำบลแสนสุข อำเภอเมืองชลบุรี ชลบุรี 20131',
    address: 'มหาวิทยาลัยบูรพา ตำบลแสนสุข อำเภอเมืองชลบุรี ชลบุรี 20131',
    serviceday: faker.date.recent(),
    work: 'ตำแหน่งพนักงานสนับสนุนวิชาการ',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    phone: '0930130931',
    educations: [
      {
        _id: new ObjectID('60015a2447fecb1090335c45'),

        name: 'รัฐประศาสนศาสตรบัณฑิต',
        abbreviation: 'รป.บ',
        major: 'วิทยาการจัดการ',
        year: '2558',
        institution: 'มหาวิทยาลัยสุโขทัยธรรมาธิราช'
      },
      {
        _id: new ObjectID('60015a378001f720b8d35345'),
        name: 'ประกาศนียบัตรวิชาชีพชั้นสูง',
        abbreviation: 'ปวส.',
        major: 'คอมพิวเตอร์ธุรกิจ',
        year: '2549',
        institution: 'โรงเรียนเทคโนโลยีชลบุรี'
      }
    ]
  },
  {
    _id: new ObjectID('5fdc5ddc8127c53ddce25701'),
    name: 'ภูเบศ',
    surname: 'ผึ้งสุก',
    email: 'user@user.com',
    password: '$2a$05$2KOSBnbb0r.0TmMrvefbluTOB735rF/KRZb4pmda4PdvU9iDvUB26',
    role: 'user',
    verified: true,
    verification: '3d6e072c-0eaf-4239-bb5e-495e6486148f',
    academicPrefix: 'ศาสตราจารย์',
    prefix: 'นาย',
    ratenumber: '091ค',
    position: 'ผู้ปฏิบัติงานห้องสมุด',
    affiliate: 'สำนักหอสมุด',
    division: 'ฝ่ายพัฒนาทรัพยากรสารสนเทศ',
    branch: '-',
    idCard: '3200100601234',
    birthdate: faker.date.past(),
    gender: 'ชาย',
    children: '-',
    religion: 'ศาสนาพุทธ',
    status: 'โสด',
    blood: 'บี',
    domicile: 'ตำบลบ้านระกาศ อำเภอบางบ่อ สมุทรปราการ 10550',
    address: 'มหาวิทยาลัยบูรพา ตำบลแสนสุข อำเภอเมืองชลบุรี ชลบุรี 20131',
    serviceday: faker.date.recent(),
    work: 'ตำแหน่งพนักงานสนับสนุนวิทยาการคอมพิวเตอร์',
    nationality: 'ไทย',
    ethnicity: 'ไทย',
    phone: '0930131234',
    educations: [
      {
        _id: new ObjectID('60015a4725290f319c378d69'),

        name: 'รัฐประศาสนศาสตรบัณฑิต',
        abbreviation: 'รป.บ',
        major: 'วิทยาการจัดการ',
        year: '2558',
        institution: 'มหาวิทยาลัยสุโขทัยธรรมาธิราช'
      },
      {
        _id: new ObjectID('60015a4d2679101b88295d33'),
        name: 'ประกาศนียบัตรวิชาชีพชั้นสูง',
        abbreviation: 'ปวส.',
        major: 'คอมพิวเตอร์ธุรกิจ',
        year: '2549',
        institution: 'โรงเรียนเทคโนโลยีชลบุรี'
      }
    ]
  }
]
