const { createProject } = require('./createProject')
const { deleteProject } = require('./deleteProject')
const { getAllProject } = require('./getAllProject')
const { getProject } = require('./getProject')
const { getProjects } = require('./getProjects')
const { updateProject } = require('./updateProject')

module.exports = {
  createProject,
  deleteProject,
  getAllProject,
  getProject,
  getProjects,
  updateProject
}
