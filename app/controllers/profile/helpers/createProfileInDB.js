const User = require('../../../models/libUser')
const { itemNotFound } = require('../../../middleware/utils')

/**
 * Updates profile in database
 * @param {Object} req - request object
 * @param {string} id - user id
 */
const createProfileInDB = (req = {}, id = '') => {
  return new Promise((resolve, reject) => {
    User.findByIdAndUpdate(
      id,
      {
        $push: {
          educations: {
            name: req.educations[0].name,
            abbreviation: req.educations[0].abbreviation,
            major: req.educations[0].major,
            year: req.educations[0].year,
            institution: req.educations[0].institution
          }
        },
        new: true,
        runValidators: true,
        select: '-role -_id -updatedAt -createdAt'
      },
      async (err, user) => {
        try {
          await itemNotFound(err, user, 'NOT_FOUND')
          resolve(user)
        } catch (error) {
          reject(error)
        }
      }
    )
  })
}

module.exports = { createProfileInDB }
