const faker = require('faker')

const json = [
    {
        name : 'เบศภู ',
        surname: 'ผึ้งสุข',
        type: 'Sick Leave',
        start: faker.date.past(),
        end: faker.date.recent(),
        quantity: 2,
        description: 'ปวดหัว ตามัว ตัวร้อน ไม่ได้พักไม่ได้ผ่อน ผอมแห้งแรงน้อย กินไม่ได้ นอนไม่หลับ',
        certificate: 'มีใบรับรองแพทย์'
    },
    {
        name : 'ใครไม่สืบ ',
        surname: 'หลวงสืบ',
        type: 'Personal Leave',
        start: faker.date.past(),
        end: faker.date.recent(),
        quantity: 5,
        description: 'กระส่ายกระสับ ตับพิการ อาหารไม่ย่อย ลิ้นกร่อย ทานอาหารไม่อร่อย ไม่รู้รส',
        certificate: 'ไม่มีใบรับรองแพทย์'
    }
]

module.exports = json
