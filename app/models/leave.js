const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const LeaveSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    surname: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    start: {
        type: Date,
        required: true
    },
    end: {
        type: Date,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    certificate: {
        type: String,
        required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)
LeaveSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Leave', LeaveSchema)
