const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const {
  sendRegistrationEmailMessage,
} = require('../../middleware/emailer')
const { createEducationInDb } = require('./helpers')

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createEducation = async (req, res) => {
  try {
    // Gets locale from header 'Accept-Language'
    req = matchedData(req)
    const item = await createEducationInDb(req)
    res.status(201).json(item)
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createEducation }
