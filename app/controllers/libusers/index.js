const { createLibuser } = require('./createLibuser')
const { deleteLibuser } = require('./deleteLibuser')
const { getLibuser } = require('./getLibuser')
const { getLibusers } = require('./getLibusers')
const { updateLibuser } = require('./updateLibuser')
const { createEducation } = require('./createEducation')


module.exports = {
  createLibuser,
  deleteLibuser,
  getLibuser,
  getLibusers,
  updateLibuser,
  createEducation
}
