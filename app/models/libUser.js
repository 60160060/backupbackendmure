const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const validator = require('validator')
const mongoosePaginate = require('mongoose-paginate-v2')

const EducationSchema = new mongoose.Schema({
  name: {
    type: String
  },
  abbreviation: {
    type: String
  },
  major: {
    type: String
  },
  year: {
    type: String
  },
  institution: {
    type: String
  }
})

const LibUserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    surname: {
      type: String,
      required: true
    },
    email: {
      type: String,
      validate: {
        validator: validator.isEmail,
        message: 'EMAIL_IS_NOT_VALID'
      },
      lowercase: true,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true,
      select: false
    },
    role: {
      type: String,
      enum: ['user', 'admin'],
      default: 'user'
    },
    verification: {
      type: String
    },
    verified: {
      type: Boolean,
      default: false
    },
    academicPrefix: {
      type: String,
      enum: ['ศาสตราจารย์', 'รองศาสตราจารย์', 'ผู้ช่วยศาสตราจารย์', 'อาจารย์']
    },
    prefix: {
      type: String,
      enum: ['นาย', 'นาง', 'นางสาว']
    },
    ratenumber: {
      type: String
    },
    position: {
      type: String
    },
    affiliate: {
      type: String
    },
    division: {
      type: String
    },
    branch: {
      type: String
    },
    idCard: {
      type: String,
      unique: true,
      required: true
    },
    birthdate: {
      type: Date
    },
    gender: {
      type: String,
      enum: ['ชาย', 'หญิง', 'ไม่ระบุ']
    },
    children: {
      type: String
    },
    religion: {
      type: String,
      enum: ['ศาสนาพุทธ', 'ศาสนาคริสต์', 'ศาสนาอิสลาม', 'ศาสนาพราหมณ์-ฮินดู']
    },
    status: {
      type: String,
      enum: ['โสด', 'สมรส', 'หม้าย', 'หย่า', 'แยกกันอยู่']
    },
    blood: {
      type: String,
      enum: ['เอ', 'บี', 'โอ', 'เอบี']
    },
    domicile: {
      type: String
    },
    address: {
      type: String
    },
    serviceday: {
      type: Date
    },
    work: {
      type: String
    },
    nationality: {
      type: String
    },
    ethnicity: {
      type: String
    },
    phone: {
      type: String
    },
    educations:[EducationSchema]
  },
  {
    versionKey: false,
    timestamps: true
  }
)

const hash = (user, salt, next) => {
  bcrypt.hash(user.password, salt, (error, newHash) => {
    if (error) {
      return next(error)
    }
    user.password = newHash
    return next()
  })
}

const genSalt = (user, SALT_FACTOR, next) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

LibUserSchema.pre('save', function (next) {
  const that = this
  const SALT_FACTOR = 5
  if (!that.isModified('password')) {
    return next()
  }
  return genSalt(that, SALT_FACTOR, next)
})

LibUserSchema.methods.comparePassword = function (passwordAttempt, cb) {
  bcrypt.compare(passwordAttempt, this.password, (err, isMatch) =>
    err ? cb(err) : cb(null, isMatch)
  )
}
LibUserSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('LibUser', LibUserSchema)
