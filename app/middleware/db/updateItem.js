const { itemNotFound } = require('../../middleware/utils')

/**
 * Updates an item in database by id
 * @param {string} id - item id
 * @param {Object} req - request object
 */
const updateItem = (id = '', model = {}, req = {}) => {
  return new Promise((resolve, reject) => {
    model.findByIdAndUpdate(
      id, 
      { $push: { 
                educations: {
                  "name" : req.educations[0].name,
                  "abbreviation" : req.educations[0].abbreviation,
                  "major" : req.educations[0].major,
                  "year" : req.educations[0].year,
                  "institution" : req.educations[0].institution,
                  }  
              } 
      },
        {
          new: true,
          runValidators: true
        },
        async (err, item) => {
          try {
            await itemNotFound(err, item, 'NOT_FOUND')
            resolve(item)
          } catch (error) {
            reject(error)
          }
        })
    // model.findByIdAndUpdate(
    //   id,
    //   req,
    //   {
    //     new: true,
    //     runValidators: true
    //   },
    //   async (err, item) => {
    //     try {
    //       await itemNotFound(err, item, 'NOT_FOUND')
    //       resolve(item)
    //     } catch (error) {
    //       reject(error)
    //     }
    //   }
    // )
  })
}

module.exports = { updateItem }
