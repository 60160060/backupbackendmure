const { changePasswordInDB } = require('./changePasswordInDB')
const { findUser } = require('./findUser')
const { getProfileFromDB } = require('./getProfileFromDB')
const { updateProfileInDB } = require('./updateProfileInDB')
const { createProfileInDB } = require('./createProfileInDB')


module.exports = {
  changePasswordInDB,
  findUser,
  getProfileFromDB,
  updateProfileInDB,
  createProfileInDB
}
