const uuid = require('uuid')
const Education = require('../../../models/education')
const { buildErrObject } = require('../../../middleware/utils')

/**
 * Creates a new item in database
 * @param {Object} req - request object
 */
const createEducationInDb = ({
    educations = []
}) => {
    return new Promise((resolve, reject) => {
      const libUser = new Education({
        educations,
        verification: uuid.v4()
      })
      libUser.save((err, item) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }

        item = JSON.parse(JSON.stringify(item))

        // delete item.password
        // delete item.blockExpires
        // delete item.loginAttempts

        resolve(item)
      })
    })
  }

  module.exports = { createEducationInDb }
