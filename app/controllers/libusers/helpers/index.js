const { createItemInDb } = require('./createItemInDb')
const { createEducationInDb } = require('./createEducationInDb')


module.exports = {
  createItemInDb,
  createEducationInDb
}
