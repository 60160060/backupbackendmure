const { changePassword } = require('./changePassword')
const { getProfile } = require('./getProfile')
const { updateProfile } = require('./updateProfile')
const { createProfile } = require('./createProfile')


module.exports = {
  changePassword,
  getProfile,
  updateProfile,
  createProfile
}
