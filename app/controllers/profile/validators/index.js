const { validateChangePassword } = require('./validateChangePassword')
const { validateUpdateProfile } = require('./validateUpdateProfile')
const { validateCreateProfile } = require('./validateCreateProfile')


module.exports = {
  validateChangePassword,
  validateUpdateProfile,
  validateCreateProfile
}
